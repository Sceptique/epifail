require 'test_helper'

class NoobsControllerTest < ActionController::TestCase
  setup do
    @noob = noobs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:noobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create noob" do
    assert_difference('Noob.count') do
      post :create, noob: {  }
    end

    assert_redirected_to noob_path(assigns(:noob))
  end

  test "should show noob" do
    get :show, id: @noob
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @noob
    assert_response :success
  end

  test "should update noob" do
    patch :update, id: @noob, noob: {  }
    assert_redirected_to noob_path(assigns(:noob))
  end

  test "should destroy noob" do
    assert_difference('Noob.count', -1) do
      delete :destroy, id: @noob
    end

    assert_redirected_to noobs_path
  end
end
