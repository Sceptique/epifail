def get_modules body
  JSON.parse(body.match(/modules:(\[[^\]]+\])/m)[1])
end

def user_page(name)
  "https://intra.epitech.eu/user/#{name}/"
end

namespace :scrap do
  task run: :environment do
    puts "The scrap is running..."
    URL = "https://intra.epitech.eu/"
    USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"    
    a = Mechanize.new
    a.user_agent = USER_AGENT

    begin
      p = a.get(URL)
    rescue Mechanize::ResponseCodeError => exception
      if exception.response_code == '403'
        p = exception.page
      else
        raise # Some other error, re-raise
      end
    end

    p.form do |f|
      f['login'] = ENV['USERNAME']
      f['password'] = ENV['PASSWORD']
    end.submit

    Noob.all.pluck(:name).each do |name|
      p = a.get(user_page(name)) rescue next
      modules = get_modules(p.body)
    end
  end
end
