class Mark < ActiveRecord::Base
  belongs_to :noob

  def noob_name
    Noob.find_by(id: noob_id).name rescue ""
  end

  def noob_name= name
    self.noob = Noob.find_by(name: name)
    puts "I'm setting the noob : #{name} => #{noob_id}"
  end

end
