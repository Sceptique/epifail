class LockBad < ActiveRecord::Base
  belongs_to :noob
  belongs_to :user

  TIMEOUT = 1.day.ago

  #validate :check_if_writable
  def check_if_writable
    return true if updated_at.nil?
    return true if updated_at < TIMEOUT
    errors.add :user, "Next avaliable for #{self.noob.name} vote after 7 days."
    return false
  end
end
