class Noob < ActiveRecord::Base
  has_many :follows
  has_many :users, through: :follows
  has_many :followers, through: :follows, source: :user, class_name: :User, foreign_key: :user_id
  has_many :marks
  self.per_page = 50

  validates :name, format: { with: /[[:alnum:]_\-]{2,10}/, message: 'only a-Z,-,_,0-9, size 2..10' }
  validates :name, uniqueness: true

  before_save :default_bad
  def default_bad
    self.bad ||= 0
  end

  def find_lock_for_user u
    LockBad.find_by(user: u, noob: self)
  end

  def locked_for_user? u
    #return false
    return false if u.admin?
    lock = find_lock_for_user u
    if lock and lock.updated_at > LockBad::TIMEOUT
      return true
    else
      return false
    end
  end

  def lock_for_user! u
    lock = find_lock_for_user u
    if lock
      return lock.update!(updated_at: Time.now)
    else
      return LockBad.create!(noob: self, user: u)
    end
  end
end
