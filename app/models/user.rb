class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         #:recoverable,
         :rememberable, :trackable, :validatable,
         :lockable#, :confirmable

  has_many :follows
  has_many :noobs, through: :follows

  before_save :validate_group, :set_default_group
  def set_default_group
    self.group = "default" if self.group.to_s.strip.empty?
  end
  def validate_group
    self.group.downcase!
    self.group.gsub!(/[^[:alnum:]_,\-]/, '')
  end

  def self.groups
    return Groups
  end

  def groups
    return group.split(',')
  end

  def admin?
    groups.include? User.groups.admin
  end

  def lock!
    self.update!(lock: self.encrypted_password, encrypted_password: 'locked')
  end

  def unlock!
    self.update!(encrypted_password: self.lock, lock: nil)
  end

  def locked?
    not self.lock.to_s.empty?
  end
  def locked
    return locked?
  end
  def locked= lock_now
    lock! if lock_now == "1" and not locked?
    unlock! if lock_now == "0" and locked?
  end
end

module Groups

  ADMIN_GROUP = 'admin'
  DEFAULT_GROUP = 'default'

  def self.admin
    return ADMIN_GROUP
  end
  def self.default
    return DEFAULT_GROUP
  end
end
