json.array!(@marks) do |mark|
  json.extract! mark, :id, :value, :noob_id, :noob_name
  json.url mark_url(mark, format: :json)
end
