Rails.application.routes.draw do

  root to: "noobs#index"

  post '/noobs/:id/follow(.:format)', action: :follow, controller: :noobs, as: :follow_noob
  post '/noobs/:id/unfollow(.:format)', action: :unfollow, controller: :noobs, as: :unfollow_noob
  post '/noobs/:id/im_bad(.:format)', action: :im_bad, controller: :noobs, as: :im_bad_noob
  get '/noobs/follows', action: :follows, controller: :noobs
  resources :noobs do
    resources :marks
  end

  resources :marks

  devise_for :users
  resources :users

end
