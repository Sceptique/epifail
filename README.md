# EpiFail

## Please use GITLAB instead of GITHUB
For merge requests, issues, bugs, discus, ... please use [gitlab](https://gitlab.com/poulet_a/epifail/).  
Gitlab is an opensource clone of github.

## What is EF ?
Epifail is a simple application that scrap the Epitech Intranet and create a database with the best epic-fails of Epitech.
The marks -84, the better comments, etc. And all is sociabilised. You can follow, report, comment a noob, etc.

## How to run ?

Like a simple Ruby On Rails Application, after configuring the config/database.yml file :  
``bundle install && rake db:create && rake db:migrate && rake db:seed && rails s``

You can also run the scrap with ``rake scrap:run`` that should run the scraper on the intranet.  
__note__ You should configure the .env too.

