class CreateNoobs < ActiveRecord::Migration
  def change
    create_table :noobs do |t|
      t.string :name, limit: 10, null: false, uniqueness: true # name is like "poulet_a"
      t.string :about # note about this noob
      t.integer :bad, limit: 4, default: 0, null: false #is he bad ? if yes + 1
      t.timestamps
    end
  end
end
