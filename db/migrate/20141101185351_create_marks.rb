class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.integer :value, limit: 4
      t.belongs_to :noob
      t.string :note
      t.timestamps
    end
    add_index :marks, [:value, :noob_id]
  end
end
