class CreateLockBads < ActiveRecord::Migration
  def change
    create_table :lock_bads do |t|
      t.references :user
      t.references :noob
      t.timestamps
    end
  end
end
