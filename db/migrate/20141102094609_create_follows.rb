class CreateFollows < ActiveRecord::Migration
  def change
    create_table :follows, id: false do |t|
      t.belongs_to :noob
      t.belongs_to :user
    end
  end
end
